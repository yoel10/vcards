from django.urls import path

from .views import send, users

urlpatterns = [
    path('email', send),
    path('users', users),
]